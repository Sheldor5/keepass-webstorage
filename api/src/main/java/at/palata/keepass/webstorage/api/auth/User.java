package at.palata.keepass.webstorage.api.auth;

import at.palata.keepass.webstorage.api.config.KeePassWebStorageConfiguration;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Simple DTO for authenticated users.
 *
 * {@link KeePassWebStorageConfiguration} provides the current request's user object as session-scoped bean at runtime.
 */
@Getter
@Setter
@ToString(exclude = "password")
@Valid
public class User implements Cloneable {

	@NotNull
	@NotBlank
	private String username;

	@NotNull
	@NotBlank
	private String password;

	@NotNull
	private Boolean backupsEnabled = true;

	@NotNull
	private Boolean updatesAllowed = true;

	@NotNull
	private Boolean isAdmin = false;

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (obj instanceof User) {
			if (this.username == null) {
				return false;
			}
			return this.username.equals(((User) obj).username);
		}
		return false;
	}

	@Override
	public User clone() {
		try {
			final User clone = (User) super.clone();
			clone.username = username;
			clone.password = password;
			clone.backupsEnabled = backupsEnabled;
			clone.updatesAllowed = updatesAllowed;
			clone.isAdmin = isAdmin;
			return clone;
		} catch (final CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
}
