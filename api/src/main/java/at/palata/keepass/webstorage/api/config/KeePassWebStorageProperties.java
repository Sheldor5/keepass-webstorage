package at.palata.keepass.webstorage.api.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Spring Boot {@link ConfigurationProperties} which holds the KeePass WebStorage configuration.
 *
 * Enable and autowire this class to access the configuration of the KeePass WebStorage at runtime.
 */
@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = KeePassWebStoragePropertiesConstants.PROPERTY_PREFIX)
@Slf4j
public final class KeePassWebStorageProperties {

	private boolean updatesAllowed = true;

	private boolean backupsEnabled = true;

	private boolean httpsRequired = true;

	private boolean adminEnabled = true;

	private boolean createUsersEnabled = true;

	@NotNull
	@NotBlank
	private String storeType;

	@NotNull
	@NotBlank
	private String authProviderType;

	@NotNull
	@NotBlank
	private String contentType = "application/x-keepass2";

	@PostConstruct
	public void initialize() {
		log.debug("Initializing {} ...", this);
		log.debug("Successfully initialized {}", this);
	}
}
