package at.palata.keepass.webstorage.api.auth;

/**
 * Security constants useful for {@link AuthenticationProvider} implementations.
 */
public final class SecurityConstants {

	/**
	 * Username of the default Administrator account.
	 */
	public static final String ADMIN_USER = "admin";

	/**
	 * Role of Administrator accounts.
	 */
	public static final String ADMIN_ROLE = "ADMIN";

	/**
	 * Authority of Administrator accounts.
	 */
	public static final String ADMIN_AUTHORITY = "ROLE_" + ADMIN_ROLE;
}
