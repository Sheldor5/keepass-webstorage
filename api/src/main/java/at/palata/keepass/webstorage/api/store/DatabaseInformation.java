package at.palata.keepass.webstorage.api.store;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Simple DTO which represents a single database backup version.
 */
@Getter
@Setter
public class DatabaseInformation {

	/**
	 * Version number of this Database backup.
	 */
	protected Integer version;

	/**
	 * Timestamp of the last modification.
	 */
	protected LocalDateTime lastModified;
}
