package at.palata.keepass.webstorage.api.config;

/**
 * Useful constants for {@code application.properties} configuration files.
 */
public final class KeePassWebStoragePropertiesConstants {
	public static final String PROPERTY_PREFIX = "keepass.webstorage";
	public static final String AUTH_PROVIDER_TYPE_PROPERTY = PROPERTY_PREFIX + ".auth-provider-type";
	public static final String STORE_TYPE_PROPERTY = PROPERTY_PREFIX + ".store-type";
}
