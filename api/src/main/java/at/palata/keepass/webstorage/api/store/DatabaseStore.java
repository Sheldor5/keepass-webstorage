package at.palata.keepass.webstorage.api.store;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

/**
 * Implement this class if you want to develop your own Database Store.
 *
 * The implementation is responsible for saving, versioning, backing up, restoring and deleting KeePass database files.
 */
@Service
public interface DatabaseStore {

	/**
	 * Get the main database.
	 *
	 * @param username the user's identifier.
	 * @return the main database or {@link Optional#empty()} if the user has no (initial) database.
	 */
	Optional<Resource> getDatabase(final String username);

	/**
	 * Get information about the main database.
	 *
	 * @param username username the user's identifier.
	 * @return information about the main database or {@link Optional#empty()} if the user has no (initial) database.
	 */
	Optional<DatabaseInformation> getDatabaseInformation(final String username);

	/**
	 * Get a specific database version.
	 *
	 * @param username the user's identifier.
	 * @param version the version of the database.
	 * @return the database version or {@link Optional#empty()}.
	 */
	Optional<Resource> getVersion(final String username, final Integer version);

	/**
	 * Get information about all database versions.
	 *
	 * @param username the user's identifier.
	 * @return a list of all database versions.
	 */
	Optional<Integer> getVersionCount(final String username);

	/**
	 * Get information about all database versions.
	 *
	 * @param username the user's identifier.
	 * @return a list of all database versions.
	 */
	Collection<DatabaseInformation> getVersions(final String username);

	/**
	 * Update the main database.
	 *
	 * @param username the user's identifier.
	 * @param newDatabase the new database as {@link InputStream}.
	 * @return the main database or {@link Optional#empty()} if ??? TODO.
	 * @param backup if a backup of the current main database should be made before updating the main database.
	 */
	Optional<Resource> updateDatabase(final String username, final InputStream newDatabase, final boolean backup);

	/**
	 * Restore a specific database version.
	 *
	 * @param username the user's identifier.
	 * @param version the version of the database.
	 * @param backup if a backup of the current main database should be made before restoring it to an older version.
	 */
	OperationResult restoreVersion(final String username, final Integer version, final boolean backup);

	/**
	 * Delete
	 *
	 * @param username the user's identifier.
	 * @param version the version of the database.
	 */
	OperationResult deleteVersion(final String username, final Integer version);

	/**
	 * Delete all database versions lesser than or equals a specific database version.
	 *
	 * @param username the user's identifier.
	 * @param from the version of the database.
	 * @param to the version of the database.
	 */
	OperationResult deleteVersionRange(final String username, final Integer from, final Integer to);
}
