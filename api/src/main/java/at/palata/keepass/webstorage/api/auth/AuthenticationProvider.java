package at.palata.keepass.webstorage.api.auth;

import at.palata.keepass.webstorage.api.store.OperationResult;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Optional;

/**
 * Extend this class if you want to develop your own Authentication Provider.
 *
 * The implementation should persist the {@link User}s in the background,
 * for example in a database or user directory.
 *
 * Only persist encoded/hashed passwords for security reasons.
 */
public abstract class AuthenticationProvider implements org.springframework.security.authentication.AuthenticationProvider {

	/**
	 * Create a new user. This method is only called by the Administrator.
	 *
	 * @param user the new user.
	 */
	public abstract OperationResult createUser(final User user);

	/**
	 * Request an user from this Authentication Provider.
	 *
	 * @param username the user's identifier.
	 * @return the user object.
	 */
	public abstract Optional<User> getUser(final String username);

	/**
	 * Update the user's attributes. This method is only called by the Administrator.
	 * The {@param newAttributes} user object contains all attributes which should be updated.
	 * If an attribute is null, do not update this attribute.
	 *
	 * @param newAttributes the new user attributes as user object.
	 */
	public abstract OperationResult updateUser(final User newAttributes);

	/**
	 * Change the user's password.
	 * The {@param oldPassword} must match the user's current password before updating it with the {@param newPassword}.
	 *
	 * @param username the user's identifier.
	 * @param oldPassword the user's current password.
	 * @param newPassword the user's new password.
	 * @return true on success, false otherwise
	 */
	public abstract OperationResult changeUserPassword(final String username, final String oldPassword, final String newPassword);

	@Override
	public final boolean supports(final Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
