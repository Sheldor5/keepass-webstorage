package at.palata.keepass.webstorage.api.config;

import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import at.palata.keepass.webstorage.api.auth.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Optional;

/**
 * Spring Boot {@link Component} to provide runtime Beans.
 *
 * Autowire the {@link User} if you need the current request's user object at runtime.
 */
@Component
@Slf4j
public class KeePassWebStorageConfiguration {

	private final AuthenticationProvider authenticationProvider;

	@Autowired
	public KeePassWebStorageConfiguration(final AuthenticationProvider authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}

	/**
	 * Session-scoped user object for easy user attribute access.
	 *
	 * @return the {@link User} of the current request.
	 */
	@Bean
	@SessionScope
	public User keePassWebStorageUser() {
		final String username = SecurityContextHolder.getContext().getAuthentication().getName();
		log.trace("Initializing session-scoped KeePassWebStorageUser for user '{}' ...", username);
		if (username == null || username.isEmpty()) {
			throw new BeanInitializationException("Username can not be null or empty");
		}

		final Optional<User> optionalUser = authenticationProvider.getUser(username);
		if (!optionalUser.isPresent()) {
			throw new BeanInitializationException("User can not be null");
		}

		final User user = optionalUser.get();
		log.trace("Successfully initialized session-scoped {}", user);
		return user;
	}
}
