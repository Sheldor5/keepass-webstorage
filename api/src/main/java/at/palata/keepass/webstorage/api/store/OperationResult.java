package at.palata.keepass.webstorage.api.store;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Getter
public class OperationResult {

	private final Status status;
	private final String message;

	private OperationResult(final Status status, final String message) {
		this.status = status;
		this.message = message;
	}

	public boolean wasSuccessful() {
		return status == Status.SUCCESS;
	}

	public boolean hadErrors() {
		return status == Status.ERROR;
	}

	public boolean wereNotFound() {
		return status == Status.NOT_FOUND;
	}

	public ResponseEntity<?> responseEntity() {
		switch (status) {
			case SUCCESS:
				return ResponseEntity.ok().build();
			case NOT_FOUND:
				return ResponseEntity.notFound().build();
			case NOT_IMPLEMENTED:
				return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(message);
			case ERROR:
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(message);
		}
		throw new IllegalStateException("status must not be null");
	}

	public static OperationResult success() {
		return new OperationResult(Status.SUCCESS, null);
	}

	public static OperationResult notFound() {
		return new OperationResult(Status.NOT_FOUND, null);
	}

	public static OperationResult notImplemented(final String message) {
		return new OperationResult(Status.NOT_IMPLEMENTED, message);
	}

	public static OperationResult error(final String message) {
		return new OperationResult(Status.ERROR, message);
	}

	private enum Status {
		SUCCESS,
		NOT_FOUND,
		NOT_IMPLEMENTED,
		ERROR
	}
}
