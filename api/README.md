# API for [_KeePass WebStorage_](https://gitlab.com/Sheldor5/keepass-webstorage)

![KeePass WebStorage](../readme/home.png)

## Tutorial - Custom Authentication Provider

[KeePass WebStorage](https://gitlab.com/Sheldor5/keepass-webstorage) requires a single Spring `@Component` of type `KeePassWebStorageAuthenticationProvider` being available at runtime.

The `KeePassWebStorageAuthenticationProvider` is responsible for authenticating users against some kind of User Management,
for example a Relational Database Management System or an LDAP User Directory.

You simply have to implement the  `KeePassWebStorageAuthenticationProvider` interface and provide it as Spring `@Component`, for example:

```java
@Component
public class MyCustomAuthenticationProvider extends KeePassWebStorageAuthenticationProvider {

	private final MyCustomUserManagement userManagement;

	@Autowired
	public MyCustomAuthenticationProvider(final MyCustomUserManagement userManagement) {
		this.userManagement = userManagement;
	}

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		final String username = authentication.getName();
		final Object password = authentication.getCredentials().toString();

		final MyCustomUser user = getUser(username);

		if (user != null && user.getPassword().matches(password)) {
			if (user.isAdmin()) {
				return new UsernamePasswordAuthenticationToken(username, password, Collections.singletonList(new SimpleGrantedAuthority(KeePassWebStorageSecurityConstants.ADMIN_AUTHORITY)));
			} else {
				return new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList());
			}
		}

		return null;
	}

	@Override
	public void createUser(final String username, final String password) {
		userManagement.createUser(username, password);
	}

	@Override
	public KeePassWebStorageUser getUser(final String username) {
		return userManagement.getUser(username);
	}

	@Override
	public void updateUser(final KeePassWebStorageUser newAttributes) {
		userManagement.updateUser(newAttributes);
	}

	@Override
	public boolean changeUserPassword(final String username, final String oldPassword, final String newPassword) {
		return userManagement.changeUserPassword(username, oldPassword, newPassword);
	}
}

```

## Tutorial - Custom Database Store
