package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

@Configuration
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = JsonAuthenticationProviderConstants.AUTH_PROVIDER_TYPE)
@EnableConfigurationProperties(JsonAuthenticationProviderProperties.class)
@Slf4j
public class JsonAuthenticationProviderConfiguration {

	private final JsonAuthenticationProviderProperties properties;

	@Autowired
	public JsonAuthenticationProviderConfiguration(final JsonAuthenticationProviderProperties properties) {
		this.properties = properties;
	}

	@Bean
	@ConditionalOnProperty(name = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE, havingValue = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_PLAIN)
	public PasswordEncoder plainPasswordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(final CharSequence rawPassword) {
				return rawPassword == null ? "" : rawPassword.toString();
			}

			@Override
			public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
				return rawPassword != null && rawPassword.equals(encodedPassword);
			}
		};
	}

	@Bean
	@ConditionalOnProperty(name = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE, havingValue = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_PBKDF2)
	public PasswordEncoder pbkdf2PasswordEncoder() {
		final JsonAuthenticationProviderProperties.Pbkdf2EncoderParameters pbkdf2EncoderParameters = properties.getPbkdf2EncoderParameters();
		return new Pbkdf2PasswordEncoder(pbkdf2EncoderParameters.getSecret(), pbkdf2EncoderParameters.getIterations(), pbkdf2EncoderParameters.getHashWidth());
	}

	@Bean
	@ConditionalOnProperty(name = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE, havingValue = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_BCRYPT, matchIfMissing = true)
	public PasswordEncoder BCryptPasswordEncoder() {
		final JsonAuthenticationProviderProperties.BCryptEncoderParameters bcryptEncoderParameters = properties.getBcryptEncoderParameters();
		return new BCryptPasswordEncoder(bcryptEncoderParameters.getStrength());
	}

	@Bean
	@ConditionalOnProperty(name = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE, havingValue = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_SCRYPT)
	public PasswordEncoder SCryptPasswordEncoder() {
		final JsonAuthenticationProviderProperties.SCryptEncoderParameters scryptEncoderParameters = properties.getScryptEncoderParameters();
		return new SCryptPasswordEncoder(scryptEncoderParameters.getCpuCost(), scryptEncoderParameters.getMemoryCost(), scryptEncoderParameters.getParallelization(), scryptEncoderParameters.getKeyLength(), scryptEncoderParameters.getSaltLength());
	}

	@Bean
	@ConditionalOnProperty(name = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE, havingValue = JsonAuthenticationProviderConstants.PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_ARGON2)
	public PasswordEncoder Argon2PasswordEncoder() {
		final JsonAuthenticationProviderProperties.Argon2EncoderParameters argon2EncoderParameters = properties.getArgon2EncoderParameters();
		return new Argon2PasswordEncoder(argon2EncoderParameters.getSaltLength(), argon2EncoderParameters.getHashLength(), argon2EncoderParameters.getParallelism(), argon2EncoderParameters.getMemory(), argon2EncoderParameters.getIterations());
	}
}
