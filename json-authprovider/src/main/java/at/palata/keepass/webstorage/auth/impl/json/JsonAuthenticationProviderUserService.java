package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import at.palata.keepass.webstorage.api.auth.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.io.File;
import java.util.Set;

@Component
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = JsonAuthenticationProviderConstants.AUTH_PROVIDER_TYPE)
@EnableConfigurationProperties(JsonAuthenticationProviderProperties.class)
@Slf4j
public class JsonAuthenticationProviderUserService {

	private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

	private final Object jsonWriteLock = new Object();
	private final JsonAuthenticationProviderProperties properties;

	private JsonAuthenticationProviderJsonFile jsonFile;

	@Autowired
	public JsonAuthenticationProviderUserService(final JsonAuthenticationProviderProperties properties) {
		this.properties = properties;
	}

	@PostConstruct
	public void initialize() {
		synchronized (jsonWriteLock) {
			try {
				load();
			} catch (final Exception e) {
				throw new BeanCreationException(this.getClass().getSimpleName(), "Error loading users from file '{}'", properties.getFile().getPath(), e);
			}
		}
	}

	@PreDestroy
	public void shutdown() {
		synchronized (jsonWriteLock) {
			try {
				save();
			} catch (final Exception e) {
				log.error("Error saving users", e);
			}
		}
	}

	public User getUser(final String username) {
		synchronized (jsonWriteLock) {
			return jsonFile.get(username);
		}
	}

	public void addUser(final User user) throws Exception {
		synchronized (jsonWriteLock) {
			jsonFile.add(user);
			save();
		}
	}

	/**
	 * @throws Exception TODO.
	 */
	public void load() throws Exception {
		synchronized (jsonWriteLock) {
			log.info("Loading users from file '{}'", properties.getFile().getPath());
			check();
			final File usersJsonFile = properties.getFile().getFile();
			final ObjectMapper jsonMapper = new ObjectMapper();
			jsonFile = jsonMapper.readValue(usersJsonFile, JsonAuthenticationProviderJsonFile.class);
			validate(jsonFile);
			jsonFile.initialize();
		}
	}

	/**
	 * @throws Exception TODO.
	 */
	public void save() throws Exception {
		synchronized (jsonWriteLock) {
			if (properties.isFileUpdateEnabled()) {
				log.info("Saving users to file '{}'", properties.getFile().getPath());
				check();
				final File usersJsonFile = properties.getFile().getFile();
				final ObjectMapper jsonMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
				jsonFile.setPasswordEncoderType(properties.getPasswordEncoderType());
				jsonMapper.writeValue(usersJsonFile, jsonFile);
			}
		}
	}

	private void check() throws Exception {
		synchronized (jsonWriteLock) {
			final File usersJsonFile = properties.getFile().getFile();
			if (!usersJsonFile.exists()) {
				throw new BeanCreationException("");
			}
			if (!usersJsonFile.canRead() || !usersJsonFile.canWrite()) {
				throw new BeanCreationException("");
			}
		}
	}

	private void validate(final JsonAuthenticationProviderJsonFile jsonAuthenticationProviderJsonFile) {
		final Set<ConstraintViolation<JsonAuthenticationProviderJsonFile>> _violations = VALIDATOR.validate(jsonAuthenticationProviderJsonFile);
		for (final ConstraintViolation<JsonAuthenticationProviderJsonFile> violation : _violations) {
			log.error("{}", violation.getPropertyPath());
		}

		for (final User user : jsonAuthenticationProviderJsonFile.getUsers()) {
			final Set<ConstraintViolation<User>> violations = VALIDATOR.validate(user);
			for (final ConstraintViolation<User> violation : violations) {
				log.error("Failed to validate user '{}': {} {}", user.getUsername(), violation.getPropertyPath(), violation.getMessage());
				jsonAuthenticationProviderJsonFile.remove(user.getUsername());
				break;
			}
			log.debug("Successfully validated user '{}'", user.getUsername());
		}
	}
}
