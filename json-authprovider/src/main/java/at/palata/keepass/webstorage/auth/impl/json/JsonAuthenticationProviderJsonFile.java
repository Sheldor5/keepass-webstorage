package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.auth.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
@Valid
public class JsonAuthenticationProviderJsonFile {

	@NotNull
	private JsonAuthenticationProviderProperties.PasswordEncoderType passwordEncoderType;

	@Valid
	private List<User> users = new ArrayList<>();

	@JsonIgnore
	private Map<String, User> userMap = new ConcurrentHashMap<>();

	public User get(final String username) {
		return userMap.get(username);
	}

	public void add(final User user) {
		users.add(user);
		userMap.put(user.getUsername(), user);
	}

	public void remove(final String username) {
		users.remove(userMap.remove(username));
	}

	public void initialize() {
		for (final User user : users) {
			userMap.put(user.getUsername(), user);
		}
	}
}
