package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;

class JsonAuthenticationProviderConstants {
	static final String AUTH_PROVIDER_TYPE = "json";
	static final String PROPERTY_PREFIX = KeePassWebStoragePropertiesConstants.PROPERTY_PREFIX + ".auth-provider." + AUTH_PROVIDER_TYPE;

	static final String PROPERTY_PASSWORD_ENCODER_TYPE = PROPERTY_PREFIX + ".password-encoder-type";
	static final String PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_PLAIN = "plain";
	static final String PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_PBKDF2 = "pbkdf2";
	static final String PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_BCRYPT = "bcrypt";
	static final String PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_SCRYPT = "scrypt";
	static final String PROPERTY_PASSWORD_ENCODER_TYPE_VALUE_ARGON2 = "argon2";
}
