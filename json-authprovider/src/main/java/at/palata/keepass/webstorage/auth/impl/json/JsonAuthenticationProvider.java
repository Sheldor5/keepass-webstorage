package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import at.palata.keepass.webstorage.api.auth.SecurityConstants;
import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.store.OperationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = JsonAuthenticationProviderConstants.AUTH_PROVIDER_TYPE)
@EnableConfigurationProperties(JsonAuthenticationProviderProperties.class)
@Slf4j
public class JsonAuthenticationProvider extends AuthenticationProvider {

	private final Map<String, AtomicInteger> wrongPasswordCount = new HashMap<>();
	private final Map<String, LocalDateTime> lockedUntil = new HashMap<>();
	private final JsonAuthenticationProviderProperties properties;
	private final JsonAuthenticationProviderUserService userService;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	public JsonAuthenticationProvider(final JsonAuthenticationProviderProperties properties, final JsonAuthenticationProviderUserService userService, final PasswordEncoder passwordEncoder) {
		this.properties = properties;
		this.userService = userService;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
		final String username = authentication.getName();
		final String userLogObj = String.format("User(%s)", username);
		log.debug("Authenticating {} ...", userLogObj);

		final Optional<User> optionalUser = getUser(username);

		if (!optionalUser.isPresent()) {
			log.warn("Failed to authenticate {}: unknown user", userLogObj);
			return null;
		}

		final User user = optionalUser.get();
		log.trace("Found {}", userLogObj);

		if (isLocked(username, userLogObj)) {
			return null;
		}

		final Object credentials = authentication.getCredentials();
		if (credentials != null) {
			log.trace("Matching passwords for {} ...", userLogObj);
			final String rawPassword = credentials.toString();
			final String encodedPassword = user.getPassword();
			if (passwordEncoder.matches(rawPassword, encodedPassword)) {
				log.trace("Passwords for {} matched", userLogObj);
				resetLock(username);
				if (SecurityConstants.ADMIN_USER.equals(username)) {
					log.debug("Successfully authenticated {} as Administrator", userLogObj);
					return new UsernamePasswordAuthenticationToken(username, rawPassword, Collections.singletonList(new SimpleGrantedAuthority(SecurityConstants.ADMIN_AUTHORITY)));
				} else {
					log.debug("Successfully authenticated {}", userLogObj);
					return new UsernamePasswordAuthenticationToken(username, rawPassword, Collections.emptyList());
				}
			} else {
				log.debug("Failed to authenticate {}: wrong password", userLogObj);
				onWrongPassword(username, userLogObj);
			}
		} else {
			log.trace("No credentials available for user {}", userLogObj);
		}

		return null;
	}

	private boolean isLocked(final String username, final String userLogObj) {
		log.debug("Checking if {} is locked ...", userLogObj);
		final LocalDateTime lockedUntil = this.lockedUntil.get(username);
		if (lockedUntil != null) {
			if (lockedUntil.isAfter(LocalDateTime.now(ZoneId.systemDefault()))) {
				log.debug("{} is still locked until {}", userLogObj, lockedUntil);
				return true;
			} else {
				log.debug("{} unlocked, was locked until {}", userLogObj, lockedUntil);
				resetLock(username);
			}
		}
		log.debug("{} is not locked", userLogObj);
		return false;
	}

	private void onWrongPassword(final String username, final String userLogObj) {
		final AtomicInteger wrongPasswordCount = this.wrongPasswordCount.get(username);
		if (wrongPasswordCount != null) {
			final int attempt = wrongPasswordCount.incrementAndGet();
			if (attempt >= properties.getAllowedAttempts()) {
				final LocalDateTime lockedUntilDateTime = LocalDateTime.now(ZoneId.systemDefault()).plusMinutes(properties.getLockedMinutes());
				log.info("{} entered a wrong password too often and is now locked until {}", userLogObj, lockedUntilDateTime);
				lockedUntil.put(username, lockedUntilDateTime);
			} else {
				log.debug("{} entered a wrong password, attempt #{}", userLogObj, attempt);
			}
		} else {
			this.wrongPasswordCount.put(username, new AtomicInteger(1));
		}
	}

	private void resetLock(final String username) {
		wrongPasswordCount.remove(username);
		lockedUntil.remove(username);
	}

	@Override
	public OperationResult createUser(final User user) {
		try {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userService.addUser(user);
		} catch (final Exception e) {
			return OperationResult.error(e.getMessage());
		}
		return OperationResult.success();
	}

	@Override
	public Optional<User> getUser(final String username) {
		final User user = userService.getUser(username);
		return Optional.ofNullable(user);
	}

	@Override
	public OperationResult updateUser(final User newAttributes) {
		log.debug("Updating user {}", newAttributes.getUsername());
		if (properties.isFileUpdateEnabled()) {
			final User currentUser = userService.getUser(newAttributes.getUsername());
			if (currentUser == null) {
				log.warn("Can not update user '{}': user not found", newAttributes.getUsername());
				return OperationResult.notFound();
			}

			final String rawPassword = newAttributes.getPassword();
			if (rawPassword != null && !rawPassword.isEmpty()) {
				currentUser.setPassword(passwordEncoder.encode(rawPassword));
			}
			currentUser.setBackupsEnabled(newAttributes.getBackupsEnabled() == null ? currentUser.getBackupsEnabled() : newAttributes.getBackupsEnabled());
			currentUser.setUpdatesAllowed(newAttributes.getUpdatesAllowed() == null ? currentUser.getUpdatesAllowed() : newAttributes.getUpdatesAllowed());
		} else {
			log.warn("Updating user information is disabled");
		}
		return OperationResult.success();
	}

	@Override
	public OperationResult changeUserPassword(final String username, final String oldPassword, final String newPassword) {
		log.debug("Change password");
		final User user = userService.getUser(username);
		if (user == null) {
			log.warn("Failed to change password: user not found");
			return OperationResult.notFound();
		}

		final String encodedPassword = user.getPassword();
		if (passwordEncoder.matches(oldPassword, encodedPassword)) {
			user.setPassword(passwordEncoder.encode(newPassword));
			log.info("Successfully changed password");
			return OperationResult.success();
		} else {
			log.info("Failed to change password: old password was invalid");
			return OperationResult.error("Failed to change password: old password was invalid");
		}
	}
}
