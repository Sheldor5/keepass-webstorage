package at.palata.keepass.webstorage.auth.impl.json;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.PathResource;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = JsonAuthenticationProviderConstants.AUTH_PROVIDER_TYPE)
@ConfigurationProperties(prefix = JsonAuthenticationProviderConstants.PROPERTY_PREFIX)
public class JsonAuthenticationProviderProperties {

	@NotNull
	private PathResource file = new PathResource("./keepass-webstorage-data/users.json");
	private boolean fileUpdateEnabled = true;
	private int allowedAttempts = 5;
	private int lockedMinutes = 5;

	private PasswordEncoderType passwordEncoderType = PasswordEncoderType.bcrypt;

	private Pbkdf2EncoderParameters pbkdf2EncoderParameters = new Pbkdf2EncoderParameters();
	private BCryptEncoderParameters bcryptEncoderParameters = new BCryptEncoderParameters();
	private SCryptEncoderParameters scryptEncoderParameters = new SCryptEncoderParameters();
	private Argon2EncoderParameters argon2EncoderParameters = new Argon2EncoderParameters();

	public enum PasswordEncoderType {
		plain,
		pbkdf2,
		bcrypt,
		scrypt,
		argon2;
	}

	@Getter
	@Setter
	public static class Pbkdf2EncoderParameters {
		private CharSequence secret = null;
		private int iterations = 256_000;
		private int hashWidth = 256;
	}

	@Getter
	@Setter
	public static class BCryptEncoderParameters {
		private int strength = 12;
	}

	@Getter
	@Setter
	public static class SCryptEncoderParameters {
		private int cpuCost = 64_000;
		private int memoryCost = 16;
		private int parallelization = 1;
		private int keyLength = 32;
		private int saltLength = 64;
	}

	@Getter
	@Setter
	public static class Argon2EncoderParameters {
		private int saltLength = 16;
		private int hashLength = 32;
		private int parallelism = 1;
		private int memory = 16_384;
		private int iterations = 4;
	}
}
