# JSON-AuthenticationProvider for [_KeePass WebStorage_](https://gitlab.com/Sheldor5/keepass-webstorage)

![KeePass WebStorage](../readme/home.png)

This Authentication Provider reads and writes users from and to a JSON-file on the filesystem. 

### Configuration

In the main `application.properties` you have to configure the `type` and `file`:

```properties
keepass.webstorage.auth-provider-type=json
keepass.webstorage.auth-provider.json.file=/mypath/users.json
```

##### Password Policies

You can configure the allowed attempts for wrong passwords and the lock time for locked users (default values):

```properties
keepass.webstorage.auth-provider.json.allowed-attempts=5
keepass.webstorage.auth-provider.json.locked-minutes=5
```

##### JSON-File

The JSON file configured by `${keepass.webstorage.auth-provider.json.file}` has to follow this structure:

```json
{
  "passwordEncoderType" : "<required password encoder type>",
  "users" : [
    {
      "username" : "<required username>",
      "password": "<required password>",
      "backupsEnabled": <optional boolean, default: true>,
      "updatesAllowed": <optional boolean, default: true>
    },
    {
      ...
    }
  ]
}
```

###### Password Encoder Types

`plain`

**TODO**

`pbkdf2`

**TODO**

`bcrypt`

**TODO**

`scrypt`

**TODO**

`argon2`

**TODO**

###### Example

```json
{
  "passwordEncoderType" : "bcrypt",
  "users" : [
    {
      "username" : "admin",
      "password": "$2y$15$rDLgL0T1oWq3WuekQ6VQdeHElEjL0sFjqGSOmohKKxsR8665nz406",
      "backupsEnabled": false,
      "updatesAllowed": false
    },
    {
      "username" : "alice",
      "password": "$2y$12$jL08QFoHDhWVisn66uENgOwTDnyaoOPdOa9ax7jVzakS3IK7I26BC"
    },
    {
      "username" : "bob",
      "password": "$2y$12$jj0My8RkzOia57uVrpy45.ikc4lVdzKFs//tI0dqkILeNu9yIXhYC"
    }
  ]
}
```
