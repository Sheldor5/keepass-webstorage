![KeePass WebStorage](readme/home.png)

# Features

### Multi-User

`KeePass WebStorage`  supports multiple users with their own KeePass Database.

### BasicAuth Protection

`KeePass 2` only supports `Basic Authentication` (or no authentication at all) for its `Open URL...` feature, `KeePass WebStorage` enforces `Basic Authentication`.

### Backup and Restore

Each KeePass Database can be backed up before modification.

Each user can delete or restore his KeePass Database backups.

### Security

- The KeePass Databases are saved as-is and are not decrypted.

- The `KeePass WebStorage` project is Open Source - build and run it on your own.

##### _For your own Safety_

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) Please use **different and strong passwords** for your `KeePass Database` (aka _Master Password_) and your `KeePass WebStorage` user.

# Configuration

Currently changing any configuration requires a restart to take effect.

The main configuration is done in `Spring Boot`'s `application.properties` file.

### Security Configuration

##### Secure Connections

Only allow encrypted HTTPS connections (`true` by default).

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) Only set to `false` in **local or trusted environments** because your password is sent in plain text and ca be stolen.

```properties
keepass.webstorage.https-required=true
```

### Store Configuration

You can enable and disable some store capabilities.

##### KeePass Database Backups

Make a backup before a user saves his KeePass databases (`true` by default).

If set to `false` the KeePass Databases are overwritten directly and cannot be restored.

```properties
keepass.webstorage.backups-enabled=true
```

##### Update KeePass Databases

Users can save their KeePass databases (`true` by default).

If set to `false` the KeePass Databases are read-only.

```properties
keepass.webstorage.updates-allowed=true
```

##### KeePass Database Stores

KeePass Database Stores are responsible for managing the KeePass Database `database.kdbx` files.

Available Stores:

- [Filesystem-Store](filestore-impl/)

##### Authentication Providers

Authentication Providers are responsible for managing the users of the `KeePass WebStorage`.

Available Authentication Providers:

- [JSON-AuthenticationProvider](jsonuser-impl/)

KeePass WebStorage is meant personal use or for your family and friends and not as public service with dynamic user registration.

# Build Requirements

- [Spring Boot Starter Autoconfigure](https://gitlab.com/Sheldor5/spring-boot-starter-autoconf)

# TODO

- Reloadable Server Configuration
- Integration Testing
