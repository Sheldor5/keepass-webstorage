function del(deleteUrl, version, conf) {
    const sure = conf === true ? confirm("Do you really want to delete all previous versions including the selected one?") : true;
    if (sure === true) {
        let xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                window.location.reload();
            }
        };
        xhr.open("DELETE", deleteUrl + "/" + version, false);
        xhr.send(null);
    }
}

function res(restoreUrl, version) {
    let xhr = new XMLHttpRequest();
    xhr.onload = function (e) {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            window.location.reload();
        }
    };
    xhr.open("POST", restoreUrl + "/" + version, false);
    xhr.send(null);
}

function select(id) {
    const copyText = document.getElementById(id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
}

function copyToClipboard(id) {
    select(id);
    document.execCommand("copy");
}

function formToObject($form){
    let array = $form.serializeArray();
    let object = {};

    $.map(array, function(n, i){
        if (n['value'] !== null && n['value'] !== '') {
            object[n['name']] = n['value'];
        }
    });

    // set unchecked checkboxes explicitly to "false" in object
    Array.prototype.forEach.call($form.find(':input'), function(element) {
        if(element.type === 'checkbox') {
            object[element.name] = element.checked;
        }
    });

    return object;
}

function logout() {
    // 2. send invalid BasicAuth credentials to reset browser credentials on 401 response
    const __basicAuthLogout = function () {
        let xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                window.location.href = contextPath;
            }
        };
        xhr.open("GET", window.location.href, true);
        xhr.setRequestHeader("Authorization", "Basic " + btoa("_invalid_:_invalid_"));
        xhr.setRequestHeader("X-Requested-By", "XMLHttpRequest");
        xhr.send(null);
    };

    // 1. send request to /logout endpoint
    const __logout = function () {
        let xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200 || xhr.status === 204) {
                    __basicAuthLogout();
                }
            }
        };
        xhr.open("GET", contextPath + "/logout", false);
        xhr.send(null);
    };

    __logout();
}
