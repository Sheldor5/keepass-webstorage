package at.palata.keepass.webstorage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = "at.palata")
@Slf4j
public class WebApplication {
	public static void main(final String[] args) {
		final ConfigurableApplicationContext appContext = SpringApplication.run(WebApplication.class, args);
		appContext.registerShutdownHook();
	}
}
