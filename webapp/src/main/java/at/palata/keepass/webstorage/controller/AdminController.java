package at.palata.keepass.webstorage.controller;

import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 *
 */
@Controller
@RequestMapping(path = AdminController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
public class AdminController extends AbstractController {

	public static final String PATH = "/admin";

	private final KeePassWebStorageProperties properties;

	@Autowired
	public AdminController(final KeePassWebStorageProperties properties) {
		super(AdminController.PATH);
		this.properties = properties;
	}

	@GetMapping(
			path = "",
			produces = MediaType.TEXT_HTML_VALUE
	)
	public String index(final Model model) {
		model.addAttribute("contextPath", getContextPath());
		model.addAttribute("server", properties);
		return "admin";
	}
}
