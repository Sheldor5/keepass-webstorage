package at.palata.keepass.webstorage.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import java.util.HashSet;
import java.util.Set;

@Configuration
@Slf4j
public class HttpFirewallConfiguration {

	/**
	 * Configure Spring Boot's firewall to accept HTTP MOVE requests
	 * so we can apply our own {@link NonRESTFulHttpMethodRequestFilter}.
	 *
	 * @return see {@link HttpFirewall}.
	 */
	@Bean
	public HttpFirewall defaultHttpFirewall() {
		final StrictHttpFirewall firewall = new StrictHttpFirewall();
		Set<String> allowedHttpMethods = new HashSet<>();
		allowedHttpMethods.add(HttpMethod.DELETE.name());
		allowedHttpMethods.add(HttpMethod.GET.name());
		allowedHttpMethods.add(HttpMethod.POST.name());
		allowedHttpMethods.add(HttpMethod.PUT.name());
		allowedHttpMethods.add("MOVE");
		firewall.setAllowedHttpMethods(allowedHttpMethods);
		return firewall;
	}
}
