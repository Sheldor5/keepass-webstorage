package at.palata.keepass.webstorage.api;

import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.dto.UserConverter;
import at.palata.keepass.webstorage.dto.UserDTOs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(UserApiController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class UserApiController extends AbstractAPIController {

	public static final String PATH = AbstractAPIController.PATH + "/user";

	private final AuthenticationProvider authenticationProvider;
	private final User user;

	@Autowired
	public UserApiController(final AuthenticationProvider authenticationProvider, final User user) {
		super(UserApiController.PATH);
		this.authenticationProvider = authenticationProvider;
		this.user = user;
	}

	@GetMapping("")
	public ResponseEntity<?> getUserSettings() {
		return ResponseEntity.ok(UserConverter.convertToUserSettingsDTO(user));
	}

	@PutMapping("")
	public ResponseEntity<?> changePassword(@RequestBody @Valid final UserDTOs.ChangePasswordDTO changePasswordDTO) {
		log.debug("Change Password: {}", changePasswordDTO);

		if (changePasswordDTO.validate()) {
			try {
				return authenticationProvider.changeUserPassword(user.getUsername(), changePasswordDTO.getOldPassword(), changePasswordDTO.getNewPassword()).responseEntity();
			} catch (final Exception e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			}
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("New Password and Confirmation Password do not match");
		}
	}
}
