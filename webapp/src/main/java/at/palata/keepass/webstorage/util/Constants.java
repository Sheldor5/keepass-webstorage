package at.palata.keepass.webstorage.util;

public final class Constants {
	public static final String REQUEST_PARAM_ACTION = "action";

	public static final String FORM_PARAM_ACTION_CHANGE_PASSWORD = "changePassword";
	public static final String FORM_PARAM_ACTION_UPDATE_USER = "updateUser";
}
