package at.palata.keepass.webstorage.dto;

import at.palata.keepass.webstorage.api.auth.User;

public class UserConverter {

	public static User convertToUser(final AdminDTOs.CreateUserDTO createUserDTO) {
		final User user = new User();
		user.setUsername(createUserDTO.getUsername());
		user.setPassword(createUserDTO.getPassword());
		user.setBackupsEnabled(createUserDTO.getBackupsEnabled());
		user.setUpdatesAllowed(createUserDTO.getUpdatesAllowed());
		return user;
	}

	public static User convertToUser(final String username, final AdminDTOs.UpdateUserDTO updateUserDTO) {
		final User user = new User();
		user.setUsername(username);
		user.setPassword(updateUserDTO.getNewPassword());
		user.setBackupsEnabled(updateUserDTO.getBackupsEnabled());
		user.setUpdatesAllowed(updateUserDTO.getUpdatesAllowed());
		return user;
	}

	public static AdminDTOs.UpdateUserDTO convertToAdminUpdateUserDTO(final User user) {
		final AdminDTOs.UpdateUserDTO updateUserDTO = new AdminDTOs.UpdateUserDTO();
		updateUserDTO.setBackupsEnabled(user.getBackupsEnabled());
		updateUserDTO.setUpdatesAllowed(user.getUpdatesAllowed());
		return updateUserDTO;
	}

	public static UserDTOs.UserSettingsDTO convertToUserSettingsDTO(final User user) {
		final UserDTOs.UserSettingsDTO userSettingsDTO = new UserDTOs.UserSettingsDTO();
		userSettingsDTO.setBackupsEnabled(user.getBackupsEnabled());
		userSettingsDTO.setUpdatesAllowed(user.getUpdatesAllowed());
		return userSettingsDTO;
	}
}
