package at.palata.keepass.webstorage.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class NonRESTFulHttpMethodRequestFilter implements Filter {

	/**
	 *  Answer HTTP MOVE requests, which are not supported by Spring Boot, with {@link HttpStatus#OK}.
	 *
	 * @param servletRequest see {@inheritDoc}.
	 * @param servletResponse see {@inheritDoc}.
	 * @param filterChain see {@inheritDoc}.
	 * @throws IOException see {@inheritDoc}.
	 * @throws ServletException see {@inheritDoc}.
	 */
	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

		if ("MOVE".equals(httpServletRequest.getMethod())) {
			final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
			log.trace("Ignoring 'MOVE {}' request with 200 OK", httpServletRequest.getRequestURI());
			httpServletResponse.setStatus(HttpStatus.OK.value());
		} else {
			filterChain.doFilter(servletRequest, servletResponse);
		}
	}
}
