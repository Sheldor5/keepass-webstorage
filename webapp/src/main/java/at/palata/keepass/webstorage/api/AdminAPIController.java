package at.palata.keepass.webstorage.api;

import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.api.store.DatabaseStore;
import at.palata.keepass.webstorage.api.store.OperationResult;
import at.palata.keepass.webstorage.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping(AdminAPIController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class AdminAPIController extends AbstractAPIController {

	public static final String PATH = AbstractAPIController.PATH + "/admin";

	private final KeePassWebStorageProperties properties;
	private final AuthenticationProvider authenticationProvider;
	private final DatabaseStore databaseStore;
	private final User admin;

	@Autowired
	public AdminAPIController(
			final KeePassWebStorageProperties properties,
			final AuthenticationProvider authenticationProvider,
			final DatabaseStore databaseStore,
			final User admin) {
		super(AdminAPIController.PATH);
		this.properties = properties;
		this.authenticationProvider = authenticationProvider;
		this.databaseStore = databaseStore;
		this.admin = admin;
	}

	@GetMapping("/user/{username:-?.+}")
	public ResponseEntity<?> searchUser(@PathVariable final String username) {
		try {
			return authenticationProvider.getUser(username).map(user ->
					ResponseEntity
							.ok().body(
							UserConverter.convertToAdminUpdateUserDTO(user)))
					.orElseGet(() ->
							ResponseEntity.notFound().build());
		} catch (final Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@PutMapping("/user/{username:-?.+}")
	public ResponseEntity<?> updateUser(@RequestBody @Valid final AdminDTOs.UpdateUserDTO userDTO, @PathVariable final String username) {
		if (!userDTO.hasValidPasswords()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Password and Confirmation Password do not match!");
		}
		try {
			return authenticationProvider.updateUser(UserConverter.convertToUser(username, userDTO)).responseEntity();
		} catch (final Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@PostMapping("/users")
	public ResponseEntity<?> createUser(@RequestBody @Valid final AdminDTOs.CreateUserDTO createUserDTO) {
		if (!createUserDTO.hasValidPasswords()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid User Data!");
		}
		try {
			final OperationResult operationResult = authenticationProvider.createUser(UserConverter.convertToUser(createUserDTO));
			if (operationResult.wasSuccessful()) {
				return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentContextPath().path(PATH + "/user/{username}").buildAndExpand(createUserDTO.getUsername()).toUri()).build();
			} else {
				return operationResult.responseEntity();
			}
		} catch (final Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@GetMapping("/server")
	public ResponseEntity<?> getServerSettings() {
		return ResponseEntity.ok(new AdminDTOs.ServerSettingsDTO(properties.isBackupsEnabled(), properties.isUpdatesAllowed()));
	}

	@PutMapping("/server")
	public ResponseEntity<?> updateServerSettings(@RequestBody @Valid final AdminDTOs.ServerSettingsDTO serverSettingsDTO) {
		properties.setBackupsEnabled(serverSettingsDTO.getBackupsEnabled());
		properties.setUpdatesAllowed(serverSettingsDTO.getUpdatesAllowed());
		return ResponseEntity.ok(new AdminDTOs.ServerSettingsDTO(properties.isBackupsEnabled(), properties.isUpdatesAllowed()));
	}
}
