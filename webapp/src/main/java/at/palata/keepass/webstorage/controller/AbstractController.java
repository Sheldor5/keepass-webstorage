package at.palata.keepass.webstorage.controller;

import org.springframework.beans.factory.annotation.Value;

/**
 * Abstract controller with common properties and methods.
 */
public abstract class AbstractController {

	@Value("${server.servlet.context-path:-}")
	private String contextPath;

	private final String controllerPath;

	protected AbstractController(final String controllerPath) {
		this.controllerPath = controllerPath;
	}

	protected String getContextPath() {
		return contextPath;
	}

	protected String getControllerPath() {
		return contextPath + controllerPath;
	}
}
