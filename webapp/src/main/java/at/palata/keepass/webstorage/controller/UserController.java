package at.palata.keepass.webstorage.controller;

import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.util.Constants;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.api.store.DatabaseInformation;
import at.palata.keepass.webstorage.api.store.DatabaseStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Optional;

@Controller
@RequestMapping(UserController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class UserController extends AbstractController {

	public static final String PATH = "/user";

	private final KeePassWebStorageProperties properties;
	private final DatabaseStore databaseStore;
	private final User user;

	@Autowired
	public UserController(
			final KeePassWebStorageProperties properties,
			final DatabaseStore databaseStore,
			final User user) {
		super(UserController.PATH);
		this.properties = properties;
		this.databaseStore = databaseStore;
		this.user = user;
	}

	@GetMapping
	public String getPage(final Model model, @RequestParam(name = Constants.REQUEST_PARAM_ACTION, required = false) final String action) {
		model.addAttribute("contextPath", getContextPath());
		model.addAttribute("url", String.format("%s/%s", getControllerPath(), user.getUsername()));

		model.addAttribute("user", user);
		model.addAttribute("server", properties);
		model.addAttribute(Constants.REQUEST_PARAM_ACTION, action == null ? "" : action);

		final Optional<Resource> optionalResource = databaseStore.getDatabase(user.getUsername());
		optionalResource.ifPresent(resource -> {
			try {
				final FileTime lastModified = Files.getLastModifiedTime(resource.getFile().toPath());
				final LocalDateTime localTime = LocalDateTime.ofInstant(lastModified.toInstant(), ZoneId.systemDefault());
				model.addAttribute("lastModified", localTime);
			} catch (final IOException e) {
				log.error("", e);
				model.addAttribute("lastModified", e.getMessage());
			}
		});

		model.addAttribute("backupCount", databaseStore.getVersionCount(user.getUsername()).orElse(0));

		return "user";
	}
}
