package at.palata.keepass.webstorage.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public final class UserDTOs {

	@Getter
	@Setter
	@ToString
	@Valid
	public static class ChangePasswordDTO {

		@NotNull
		@NotBlank
		private String oldPassword;

		@NotNull
		@NotBlank
		private String newPassword;

		@NotNull
		@NotBlank
		private String confirmPassword;

		public boolean validate() {
			return newPassword != null && !newPassword.isEmpty() && newPassword.equals(confirmPassword);
		}
	}

	@Getter
	@Setter
	@ToString
	@Valid
	public static class UserSettingsDTO {

		@NotNull
		private Boolean backupsEnabled;

		@NotNull
		private Boolean updatesAllowed;
	}
}
