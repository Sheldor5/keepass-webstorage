package at.palata.keepass.webstorage.api;

import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.api.store.DatabaseInformation;
import at.palata.keepass.webstorage.api.store.DatabaseStore;
import at.palata.keepass.webstorage.exception.InternalServerErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping(DatabaseAPIController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class DatabaseAPIController extends AbstractAPIController {

	public static final String PATH = AbstractAPIController.PATH + "/database";

	private final KeePassWebStorageProperties properties;
	private final DatabaseStore databaseStore;
	private final User user;

	@Autowired
	public DatabaseAPIController(
			final KeePassWebStorageProperties properties,
			final DatabaseStore databaseStore,
			final User user) {
		super(DatabaseAPIController.PATH);
		this.properties = properties;
		this.databaseStore = databaseStore;
		this.user = user;
	}

	@GetMapping
	public ResponseEntity<Resource> getDatabase() {
		try {
			return responseEntity(databaseStore.getDatabase(user.getUsername()));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while loading the database", e);
			throw new InternalServerErrorException(e);
		}
	}

	@GetMapping("/info")
	public ResponseEntity<DatabaseInformation> getDatabaseInformation() {
		try {
			return databaseStore.getDatabaseInformation(user.getUsername())
					.map(ResponseEntity::ok)
					.orElseGet(() -> ResponseEntity.notFound().build());
		} catch (final Exception e) {
			log.error("An unexpected error occurred while loading the database information", e);
			throw new InternalServerErrorException(e);
		}
	}

	@GetMapping("/versions")
	public ResponseEntity<Collection<DatabaseInformation>> getVersions() {
		try {
			return ResponseEntity.ok().body(databaseStore.getVersions(user.getUsername()));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while loading database versions", e);
			throw new InternalServerErrorException(e);
		}
	}

	@GetMapping("/version/{version:\\d+}")
	public ResponseEntity<Resource> getVersion(@PathVariable final Integer version) {
		try {
			return responseEntity(databaseStore.getVersion(user.getUsername(), version));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while loading database version {}", version, e);
			throw new InternalServerErrorException(e);
		}
	}

	@PutMapping("/version/{version:\\d+}")
	public ResponseEntity<?> restoreVersion(@PathVariable final Integer version) {
		try {
			return  databaseStore.restoreVersion(user.getUsername(), version, properties.isBackupsEnabled() && user.getBackupsEnabled()).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while restoring database version {}", version, e);
			throw new InternalServerErrorException(e);
		}
	}

	@DeleteMapping("/version/{version:\\d+}")
	public ResponseEntity<?> deleteVersion(@PathVariable final Integer version) {
		try {
			return databaseStore.deleteVersion(user.getUsername(), version).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while deleting database version {}", version, e);
			throw new InternalServerErrorException(e);
		}
	}

	@DeleteMapping("/version/{from:\\d+}/{to:\\d+}")
	public ResponseEntity<?> deleteVersion(@PathVariable final Integer from, @PathVariable final Integer to) {
		try {
			return databaseStore.deleteVersionRange(user.getUsername(), from == null ? 0 : from, to).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while deleting database versions from {} to {}", from, to, e);
			throw new InternalServerErrorException(e);
		}
	}

	private ResponseEntity<Resource> responseEntity(final Optional<Resource> optionalResource) {
		return optionalResource.map(resource ->
				ResponseEntity
						.ok()
						.header(HttpHeaders.CONTENT_TYPE, properties.getContentType())
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
						.body(resource))
				.orElseGet(() ->
						ResponseEntity.notFound().build());
	}
}
