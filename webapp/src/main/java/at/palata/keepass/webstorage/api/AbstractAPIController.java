package at.palata.keepass.webstorage.api;

import org.springframework.beans.factory.annotation.Value;

/**
 * Abstract controller with common properties and methods.
 */
public class AbstractAPIController {

	public static final String PATH = "/api";

	@Value("${server.servlet.context-path:-}")
	private String contextPath;

	private final String controllerPath;

	protected AbstractAPIController(final String controllerPath) {
		this.controllerPath = controllerPath;
	}

	protected String getContextPath() {
		return contextPath;
	}

	protected String getControllerPath() {
		return contextPath + controllerPath;
	}
}
