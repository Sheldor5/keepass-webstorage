package at.palata.keepass.webstorage.auth;

import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class KeePassWebStorageBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

	private static final String BASIC_AUTH_REALM = "KeePass WebStorage";
	public static final String BASIC_AUTH_RESPONSE_HEADER_NAME = "WWW-Authenticate";
	public static final String BASIC_AUTH_RESPONSE_HEADER_VALUE = "Basic realm=\"" + BASIC_AUTH_REALM + "\"";
	private static final String REQUESTED_BY_REQUEST_HEADER_NAME = "X-Requested-By";
	private static final String IGNORED_REQUESTED_BY_HEADER_VALUE = "XMLHttpRequest";

	@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authEx) {
		final String requestedBy = request.getHeader(REQUESTED_BY_REQUEST_HEADER_NAME);
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		if (!IGNORED_REQUESTED_BY_HEADER_VALUE.equals(requestedBy)) {
			response.addHeader(BASIC_AUTH_RESPONSE_HEADER_NAME, BASIC_AUTH_RESPONSE_HEADER_VALUE);
		}
	}

	@Override
	public void afterPropertiesSet() {
		setRealmName(BASIC_AUTH_REALM);
		super.afterPropertiesSet();
	}
}
