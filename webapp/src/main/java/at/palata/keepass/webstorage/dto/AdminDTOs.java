package at.palata.keepass.webstorage.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public final class AdminDTOs {

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Valid
	public static final class CreateUserDTO {

		@NotNull
		@NotBlank
		private String username;

		@NotNull
		@NotBlank
		private String password;

		@NotNull
		@NotBlank
		private String confirmPassword;

		@NotNull
		private Boolean backupsEnabled;

		@NotNull
		private Boolean updatesAllowed;

		public final boolean hasValidPasswords() {
			return password.equals(confirmPassword);
		}
	}

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Valid
	public static final class SearchUserDTO {

		@NotNull
		@NotBlank
		private String username;
	}

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Valid
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static final class UpdateUserDTO {

		private String newPassword;
		private String confirmPassword;

		@NotNull
		private Boolean backupsEnabled;

		@NotNull
		private Boolean updatesAllowed;

		public final boolean hasValidPasswords() {
			if (newPassword == null) {
				return confirmPassword == null;
			}

			if (newPassword.isEmpty()) {
				return false;
			}
			return newPassword.equals(confirmPassword);
		}
	}

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Valid
	public static final class ServerSettingsDTO {

		@NotNull
		private Boolean backupsEnabled;

		@NotNull
		private Boolean updatesAllowed;

	}
}
