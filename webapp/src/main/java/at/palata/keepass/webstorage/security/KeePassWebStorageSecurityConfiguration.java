package at.palata.keepass.webstorage.security;

import at.palata.keepass.webstorage.api.AbstractAPIController;
import at.palata.keepass.webstorage.api.AdminAPIController;
import at.palata.keepass.webstorage.api.auth.SecurityConstants;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.auth.KeePassWebStorageBasicAuthenticationEntryPoint;
import at.palata.keepass.webstorage.controller.AdminController;
import at.palata.keepass.webstorage.controller.KeePassOpenURLController;
import at.palata.keepass.webstorage.controller.IndexController;
import at.palata.keepass.webstorage.controller.UserController;
import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class KeePassWebStorageSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String LOGOUT_URL = "/logout";

	private KeePassWebStorageProperties keePassWebStorageProperties;
	private KeePassWebStorageBasicAuthenticationEntryPoint keePassWebStorageBasicAuthenticationEntryPoint;
	private final AuthenticationProvider authenticationProvider;

	@Autowired
	public KeePassWebStorageSecurityConfiguration(final KeePassWebStorageProperties keePassWebStorageProperties,
	                                              final KeePassWebStorageBasicAuthenticationEntryPoint keePassWebStorageBasicAuthenticationEntryPoint,
												  final AuthenticationProvider authenticationProvider) {
		this.keePassWebStorageProperties = keePassWebStorageProperties;
		this.keePassWebStorageBasicAuthenticationEntryPoint = keePassWebStorageBasicAuthenticationEntryPoint;
		this.authenticationProvider = authenticationProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// redirect to SSL connection if configured
		if (keePassWebStorageProperties.isHttpsRequired()) {
			http.requiresChannel().anyRequest().requiresSecure();
		}

		// disable CSRF
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry reg = http.csrf().disable().authorizeRequests();

		// protect database location and user details page
		reg = reg.antMatchers(KeePassOpenURLController.PATH + "/**").authenticated();
		reg = reg.antMatchers(UserController.PATH).authenticated();
		reg = reg.antMatchers(AbstractAPIController.PATH + "/**").authenticated();

		// make admin page accessible to admin only
		reg = reg.antMatchers(AdminController.PATH).hasRole(SecurityConstants.ADMIN_ROLE);
		reg = reg.antMatchers(AdminAPIController.PATH + "/**").hasRole(SecurityConstants.ADMIN_ROLE);

		// enable HTTP BasicAuth authentication
		http = reg.and().httpBasic().authenticationEntryPoint(keePassWebStorageBasicAuthenticationEntryPoint).and();

		// enable and configure logout endpoint
		http.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT_URL))
				.logoutSuccessUrl(IndexController.PATH)
				.invalidateHttpSession(true)
				.clearAuthentication(true)
				.deleteCookies("JSESSIONID");
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
		super.configure(auth);
	}
}
