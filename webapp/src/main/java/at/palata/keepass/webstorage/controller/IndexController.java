package at.palata.keepass.webstorage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = IndexController.PATH)
public class IndexController extends AbstractController {

	public static final String PATH = "/";

	public IndexController() {
		super(IndexController.PATH);
	}

	@GetMapping
	public String getIndex() {
		return "index";
	}
}
