package at.palata.keepass.webstorage.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(path = CustomErrorController.PATH)
public class CustomErrorController implements ErrorController {

	public static final String PATH = "/error";

	@GetMapping
	public String handleError(final HttpServletRequest request, final Model model) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		model.addAttribute("status", request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
		model.addAttribute("message", request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
		if (status != null) {
			return status.toString();
		}
		return "error";
	}

	@Override
	public String getErrorPath() {
		return CustomErrorController.PATH;
	}
}
