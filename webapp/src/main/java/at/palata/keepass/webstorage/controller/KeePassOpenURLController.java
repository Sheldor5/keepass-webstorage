package at.palata.keepass.webstorage.controller;

import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import at.palata.keepass.webstorage.exception.ForbiddenException;
import at.palata.keepass.webstorage.api.store.DatabaseStore;
import at.palata.keepass.webstorage.exception.InternalServerErrorException;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Optional;

/**
 * KeePass 2 does several requests on URL Locations.
 * <p>
 * KeePass 2 HTTP methods (works this way -> and we handle it this way):
 * 1. GET (check remote /database file for changes -> return original file)
 * 2. PUT (save new /database.tmp file on remote location -> update existing file)
 * 3. GET (check remote /database file for changes again -> return update file)
 * 4. DELETE (delete original /database file on remote location -> ignore and return HTTP 200 OK) (already handled by 2.)
 * 5. MOVE (move new /database.tmp file to /database file to match original location -> ignore and return HTTP 200 OK) (already handled by 2.)
 * <p>
 * We need a separate sub-path for each user because KeePass 2 gets confused if it wants to open a different databases
 * (returned by a different BasicAuth user) from the exact same URL because it does NOT open the newly requested database.
 * Seems that if the URL stays the same KeePass 2 thinks it has already opened the database
 * even if the new downloaded database does not match the currently open database ... wtf.
 */
@Controller
@Hidden
@RequestMapping(KeePassOpenURLController.PATH)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class KeePassOpenURLController extends AbstractController {

	public static final String PATH = "/database";

	private final KeePassWebStorageProperties properties;
	private final DatabaseStore databaseStore;
	private final User user;

	@Autowired
	public KeePassOpenURLController(
			final KeePassWebStorageProperties properties,
			final DatabaseStore databaseStore,
			final User user) {
		super(KeePassOpenURLController.PATH);
		this.properties = properties;
		this.databaseStore = databaseStore;
		this.user = user;
	}

	@GetMapping("/{username}")
	@ResponseBody
	public ResponseEntity<Resource> getDatabase(@PathVariable final String username, final Principal principal) {
		check(username, principal);
		try {
			return responseEntity(databaseStore.getDatabase(principal.getName()));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while loading the database", e);
			throw new InternalServerErrorException(e);
		}
	}

	@GetMapping("/{username}/{version:\\d+}")
	@ResponseBody
	public ResponseEntity<Resource> getDatabase(@PathVariable final String username, @PathVariable final Integer version, final Principal principal) {
		check(username, principal);
		try {
			return responseEntity(databaseStore.getVersion(principal.getName(), version));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while updating the database", e);
			throw new InternalServerErrorException(e);
		}
	}

	@PutMapping(
			path = "/{username}.tmp",
			consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> updateDatabase(@PathVariable final String username, final HttpServletRequest request, final Principal principal) {
		check(username, principal);
		if (!properties.isUpdatesAllowed() || !user.getUpdatesAllowed()) {
			return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
		}

		try {
			return responseEntity(databaseStore.updateDatabase(principal.getName(), request.getInputStream(), properties.isBackupsEnabled() && user.getBackupsEnabled()));
		} catch (final Exception e) {
			log.error("An unexpected error occurred while updating the database", e);
			throw new InternalServerErrorException(e);
		}
	}

	@PostMapping("/{username}/{version:\\d+}")
	public ResponseEntity<?> restoreDatabase(@PathVariable final String username, @PathVariable final Integer version, final Principal principal) {
		check(username, principal);
		try {
			return databaseStore.restoreVersion(principal.getName(), version, properties.isBackupsEnabled() && user.getBackupsEnabled()).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while updating the database", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@DeleteMapping("/{username}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> deleteDatabase(@PathVariable final String username, final Principal principal) {
		check(username, principal);
		// ignore delete requests, just response with 200 OK
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{username}/{version:-?\\d+}")
	public ResponseEntity<?> deleteDatabaseVersion(@PathVariable final String username, @PathVariable final Integer version, final Principal principal) {
		check(username, principal);
		try {
			return databaseStore.deleteVersion(principal.getName(), version).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while deleting database version", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@DeleteMapping("/{username}/version/{from:\\d+}/{to:\\d+}")
	public ResponseEntity<?> deleteVersion(@PathVariable final String username, @PathVariable final Integer from, @PathVariable final Integer to, final Principal principal) {
		check(username, principal);
		try {
			return databaseStore.deleteVersionRange(principal.getName(), from, to).responseEntity();
		} catch (final Exception e) {
			log.error("An unexpected error occurred while deleting database version", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	private void check(final String username, final Principal principal) {
		if (username == null || principal == null) {
			throw new ForbiddenException();
		}
		if (!username.equals(principal.getName())) {
			throw new ForbiddenException();
		}
	}

	private ResponseEntity<Resource> responseEntity(final Optional<Resource> optionalResource) {
		return optionalResource.map(resource ->
				ResponseEntity
						.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
						.header(HttpHeaders.CONTENT_TYPE, properties.getContentType())
						.body(resource))
				.orElseGet(() ->
						ResponseEntity.notFound().build());
	}
}
