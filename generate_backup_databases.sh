#!/bin/bash

for i in {1..100}; do
  echo "database.$(printf "%0.9d" "${i}").kdbx"
  cp -f "database.kdbx" "database.$(printf "%0.9d" "${i}").kdbx"
done
