# Filesystem-Store for [_KeePass WebStorage_](https://gitlab.com/Sheldor5/keepass-webstorage)

![KeePass WebStorage](../readme/home.png)

This store reads and writes the KeePass databases to the local filesystem.

### Configuration

In the main `application.properties` you have to configure the `type` and `directory`:

```properties
keepass.webstorage.store-type=file
keepass.webstorage.store.file.directory=/mystorage/mypath
```

For each user there will be a subdirectory created in `${keepass.webstorage.store.file.directory}` containing the user's database and backups.

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) **The initial KeePass database for each user is empty and is encrypted with the default password `changeit`.**

Either set ``keepass.webstorage.updates-allowed`` to ``true`` (to update the database with a new password) or save the KeePass database as `${keepass.webstorage.store.file.directory}/<username>/database.kdbx` on the server.
