package at.palata.keepass.webstorage.store.impl.filesystem;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.regex.Matcher;

@Slf4j
class FilesystemDatabaseStoreStoreUtils {

	public static Optional<Integer> getDatabaseVersion(final String databaseVersionFilename) {
		log.trace("Extracting database version from filename '{}'", databaseVersionFilename);
		final Matcher matcher = FilesystemDatabaseStoreConstants.EXTRACT_VERSION_PATTERN.matcher(databaseVersionFilename);
		if (matcher.find()) {
			log.trace("Filename '{}' matches pattern '{}'", databaseVersionFilename, FilesystemDatabaseStoreConstants.EXTRACT_VERSION_PATTERN.pattern());
			if (matcher.groupCount() == 1) {
				final String versionNumber = matcher.group(1);
				log.trace("Matcher found pattern group '{}' for the version number of filename '{}'", versionNumber, databaseVersionFilename);
				try {
					return Optional.of(Integer.parseInt(versionNumber));
				} catch (final NumberFormatException e) {
					log.error("A NumberFormatException occurred while parsing the version number from filename '{}'", databaseVersionFilename, e);
				}
			} else {
				log.error("Matcher found no pattern group for the version number of filename '{}' with pattern '{}'", databaseVersionFilename, FilesystemDatabaseStoreConstants.EXTRACT_VERSION_PATTERN.pattern());
			}
		} else  {
			log.error("Could not extract database version from filename '{}', filename does not match pattern '{}'", databaseVersionFilename, FilesystemDatabaseStoreConstants.EXTRACT_VERSION_PATTERN.pattern());
		}
		return Optional.empty();
	}

	public static String getDatabaseVersionFilename(final Integer version) {
		return String.format(FilesystemDatabaseStoreConstants.VERSION_FILE_NAME_FORMAT, version);
	}

	public static Optional<LocalDateTime> lastModified(final File file) {
		if (file == null) {
			log.warn("Could not read last modified timestamp of file: file is null");
		} else if (!file.exists()) {
			log.error("Could not read last modified timestamp of file '{}': file does not exist", file.getAbsolutePath());
		} else if (!file.canRead()) {
			log.error("Could not read last modified timestamp of file '{}': file is not readable", file.getAbsolutePath());
		} else {
			try {
				log.trace("Reading last modified timestamp of file '{}'", file.getAbsolutePath());
				final FileTime fileTime = Files.getLastModifiedTime(file.toPath());
				final LocalDateTime localDateTime = LocalDateTime.ofInstant(fileTime.toInstant(), ZoneId.systemDefault());
				log.debug("Successfully read last modified timestamp of file '{}': (FileTime) {} -> (LocalDateTime) {}", file.getAbsolutePath(), fileTime, localDateTime);
				return Optional.of(localDateTime);
			} catch (final IOException e) {
				log.error("An IOException occurred while reading last modified timestamp of file '{}'", file.getAbsolutePath(), e);
			}
		}
		return Optional.empty();
	}

	public static boolean deleteDirectory(final File directory) {
		try {
			deleteDirectory(directory, true);
			return true;
		} catch (final IOException e) {
			log.error("An IOException occurred while deleting '{}'", directory.getAbsolutePath(), e);
		}
		return false;
	}

	private static void deleteDirectory(final File directory, final boolean rootCall) throws IOException {
		if (directory == null) {
			log.warn("Could not recursively delete directory, directory is null");
			return;
		}
		if (!directory.exists()) {
			log.warn("Could not recursively delete directory '{}', directory does not exist", directory.getAbsolutePath());
			return;
		}
		if (rootCall) {
			log.debug("Recursively deleting directory '{}'", directory.getAbsolutePath());
		}
		if (directory.isDirectory()) {
			if (!directory.canRead()) {
				log.warn("Could not recursively delete directory '{}', directory is not readable", directory.getAbsolutePath());
				return;
			}
			if (!directory.canWrite()) {
				log.warn("Could not recursively delete directory '{}', directory is not writeable", directory.getAbsolutePath());
				return;
			}
			final File[] allContents = directory.listFiles();
			if (allContents != null) {
				for (File file : allContents) {
					try {
						deleteDirectory(file, false);
					} catch (final IOException e) {
						log.error("An IOException occurred while deleting '{}'", file.getAbsolutePath(), e);
					}
				}
			}
		}
		log.debug("# Deleting '{}'", directory.getAbsolutePath());
		Files.delete(directory.toPath());
	}
}
