package at.palata.keepass.webstorage.store.impl.filesystem;

import lombok.ToString;

import java.io.File;

@ToString(callSuper = true)
class UserDatabaseVersion extends File {
	private final int version;

	UserDatabaseVersion(final UserDirectory parent, final int version) {
		super(parent, FilesystemDatabaseStoreStoreUtils.getDatabaseVersionFilename(version));
		this.version = version;
	}
}
