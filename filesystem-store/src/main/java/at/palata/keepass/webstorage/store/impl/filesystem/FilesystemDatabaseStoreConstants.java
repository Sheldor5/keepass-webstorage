package at.palata.keepass.webstorage.store.impl.filesystem;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;

import java.io.FilenameFilter;
import java.util.regex.Pattern;

class FilesystemDatabaseStoreConstants {
	static final String STORE_NAME = "FilesystemStore";

	static final String STORE_TYPE = "filesystem";
	static final String PROPERTY_PREFIX = KeePassWebStoragePropertiesConstants.PROPERTY_PREFIX + ".store." + STORE_TYPE;

	static final String DATABASE_FILE_NAME = "database.kdbx";
	static final int VERSION_DIGITS = 9;
	static final String VERSION_FILE_NAME_FORMAT = "database.%0" + VERSION_DIGITS + "d.kdbx";
	static final FilenameFilter FILENAME_FILTER = (dir, name) -> name.matches("^database\\.\\d{" + VERSION_DIGITS + "}\\.kdbx$");
	static final Pattern EXTRACT_VERSION_PATTERN = Pattern.compile(".*database\\.(\\d{" + VERSION_DIGITS + "})\\.kdbx$");
}
