package at.palata.keepass.webstorage.store.impl.filesystem;

import lombok.ToString;

import java.io.File;

@ToString(callSuper = true)
class UserDirectory extends File {

	UserDirectory(final String parent, final String child) {
		super(parent, child);
	}
}
