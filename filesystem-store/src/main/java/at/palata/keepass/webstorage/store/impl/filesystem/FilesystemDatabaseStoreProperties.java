package at.palata.keepass.webstorage.store.impl.filesystem;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import at.palata.keepass.webstorage.api.config.KeePassWebStorageProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.PathResource;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;

@Getter
@Setter
@ToString
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.STORE_TYPE_PROPERTY, havingValue = FilesystemDatabaseStoreConstants.STORE_TYPE)
@ConfigurationProperties(prefix = FilesystemDatabaseStoreConstants.PROPERTY_PREFIX)
@EnableConfigurationProperties(KeePassWebStorageProperties.class)
@Slf4j
public class FilesystemDatabaseStoreProperties {

	@NotNull
	private PathResource directory = new PathResource("./keepass-webstorage-data");

	private File databaseDirectory;

	@PostConstruct
	public void initialize() {
		log.debug("Initializing {} ...", this);

		try {
			databaseDirectory = directory.getFile();
		} catch (IOException e) {
			throw new BeanInitializationException(String.format("Failed to initialize %s(%s): configuration value '%s' is no valid Path", FilesystemDatabaseStoreConstants.STORE_NAME, directory.getPath(), directory.getPath()));
		}

		if (!databaseDirectory.exists()) {
			throw new BeanInitializationException(String.format("Failed to initialize %s: directory does not exist", this));
		}

		if (!databaseDirectory.canRead()) {
			throw new BeanInitializationException(String.format("Failed to initialize %s: directory is not readable", this));
		}

		if (!databaseDirectory.canWrite()) {
			throw new BeanInitializationException(String.format("Failed to initialize %s: directory is not writeable", this));
		}

		log.info("Successfully initialized {}", this);
	}
}
