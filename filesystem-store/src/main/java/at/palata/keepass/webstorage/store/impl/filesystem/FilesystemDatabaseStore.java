package at.palata.keepass.webstorage.store.impl.filesystem;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.store.DatabaseInformation;
import at.palata.keepass.webstorage.api.store.DatabaseStore;
import at.palata.keepass.webstorage.api.store.OperationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;

@Service
@SessionScope
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.STORE_TYPE_PROPERTY, havingValue = FilesystemDatabaseStoreConstants.STORE_TYPE)
@EnableConfigurationProperties(FilesystemDatabaseStoreProperties.class)
@Slf4j
public class FilesystemDatabaseStore implements DatabaseStore {

	private static final String EMPTY_DATABASE_PATH = "/" + FilesystemDatabaseStoreConstants.DATABASE_FILE_NAME;

	private final String databaseDirectory;
	private final User user;
	private UserDirectory userDirectory;
	private UserDatabase userDatabase;

	@Autowired
	public FilesystemDatabaseStore(final FilesystemDatabaseStoreProperties fileStoreProperties,
								   final User user) {
		this.databaseDirectory = fileStoreProperties.getDirectory().getPath();
		this.user = user;
	}

	@PostConstruct
	public void initialize() {
		log.trace("Initializing {} ...", UserDirectory.class.getSimpleName());

		userDirectory = new UserDirectory(databaseDirectory, user.getUsername());
		log.debug("Initializing {} ...", userDirectory);

		if (!userDirectory.exists()) {
			log.debug("{} does not exist, creating new one ...", userDirectory);
			if (!userDirectory.mkdirs()) {
				throw new BeanInitializationException(String.format("Failed to initialize %s: directory could not be created", userDirectory));
			}
			log.info("Successfully created new {}", userDirectory);
		} else {
			log.trace("{} exists", userDirectory);
		}

		if (!userDirectory.canRead()) {
			throw new BeanInitializationException(String.format("Failed to initialize %s: not readable", userDirectory));
		}

		if (!userDirectory.canWrite()) {
			throw new BeanInitializationException(String.format("Failed to initialize %s: not writeable", userDirectory));
		}

		userDatabase = new UserDatabase(userDirectory, FilesystemDatabaseStoreConstants.DATABASE_FILE_NAME);
		final Optional<Path> optionalDatabase = this.getDatabase(false);
		if (!optionalDatabase.isPresent()) {
			log.debug("{} does not exist, creating initial UserDatabase ...", userDatabase);
			InputStream emptyUserDatabase;

			log.trace("Trying to find '{}' on classpath through Class ...", EMPTY_DATABASE_PATH);
			emptyUserDatabase = this.getClass().getResourceAsStream(EMPTY_DATABASE_PATH);
			if (emptyUserDatabase == null) {
				log.trace("Trying to find '{}' on classpath through Classloader ...", EMPTY_DATABASE_PATH);
				emptyUserDatabase = this.getClass().getClassLoader().getResourceAsStream(EMPTY_DATABASE_PATH);
			}

			if (emptyUserDatabase == null) {
				throw new BeanInitializationException(String.format("Could not create initial %s: could not find resource '%s' on classpath", userDatabase, EMPTY_DATABASE_PATH));
			} else {
				try {
					Files.copy(emptyUserDatabase, Paths.get(userDirectory.getAbsolutePath(), FilesystemDatabaseStoreConstants.DATABASE_FILE_NAME));
					log.debug("Successfully created initial {}", userDatabase);
				} catch (final IOException e) {
					throw new BeanInitializationException(String.format("An IOException occurred while creating initial %s", userDatabase), e);
				}
			}
		} else {
			log.trace("{} exists", userDatabase);
		}
		log.debug("Successfully initialized {}", userDirectory);
	}

	@PreDestroy
	public void destroy() {
		log.debug("Destroying {} ...", userDirectory);
		log.debug("Successfully destroyed {}", userDirectory);
	}

	@Override
	public Optional<Resource> getDatabase(final String username) {
		return getDatabase(true).map(FileSystemResource::new);
	}

	@Override
	public Optional<DatabaseInformation> getDatabaseInformation(final String username) {
		try {
			return Optional.of(_getDatabaseInformation(FilesystemDatabaseStoreConstants.DATABASE_FILE_NAME));
		} catch (final Exception e) {
			return Optional.empty();
		}
	}

	@Override
	public Optional<Resource> getVersion(final String username, final Integer version) {
		return getVersion(version).map(userDatabaseVersion -> new FileSystemResource(userDatabaseVersion.getAbsolutePath()));
	}

	@Override
	public Optional<Integer> getVersionCount(String username) {
		final String[] databaseVersions = userDirectory.list(FilesystemDatabaseStoreConstants.FILENAME_FILTER);
		if (databaseVersions == null) {
			return Optional.empty();
		}
		return Optional.of(databaseVersions.length);
	}

	@Override
	public Collection<DatabaseInformation> getVersions(String username) {
		log.trace("Listing all database backups ...");
		final String[] databaseVersions = userDirectory.list(FilesystemDatabaseStoreConstants.FILENAME_FILTER);
		if (databaseVersions == null || databaseVersions.length == 0) {
			log.debug("{} contains no database backups", userDirectory);
			return Collections.emptyList();
		}

		if (log.isTraceEnabled()) {
			final List<String> sortedVersions = Arrays.stream(databaseVersions).sorted().collect(Collectors.toList());
			log.trace("{} contains {} database backups:", userDirectory, sortedVersions.size());
			log.trace("|    Version | File");
			for (int i = 0; i < sortedVersions.size(); i++) {
				log.trace("|  {} | {}", String.format("% 9d", i + 1), sortedVersions.get(i));
			}
			return sortedVersions.stream().map(this::_getDatabaseInformation).collect(Collectors.toList());
		}

		final List<DatabaseInformation> list = Arrays.stream(databaseVersions).sorted().map(this::_getDatabaseInformation).collect(Collectors.toList());
		log.debug("{} contains {} database backups:", userDirectory, list.size());
		return list;
	}

	@Override
	public Optional<Resource> updateDatabase(final String username, final InputStream newDatabase, boolean backup) {
		log.debug("Updating {} ...", userDatabase);
		final Optional<Path> optionalDatabase = getDatabase(true);
		final Path database = optionalDatabase.orElseThrow(() -> new IllegalStateException(String.format("Failed to update %s: current database could not be found", userDatabase)));
		if (backup) {
			try {
				backup();
			} catch (final IOException e) {
				log.error("An IOException occurred while backing up the current database: {}", e.getMessage());
				throw new IllegalStateException(e);
			}
		} else {
			log.debug("Backups are disabled for {}", userDatabase);
		}
		log.trace("Writing {} to {}", newDatabase, userDatabase);
		try {
			Files.copy(newDatabase, database, StandardCopyOption.REPLACE_EXISTING);
		} catch (final IOException e) {
			log.error("An IOException occurred while updating the current database: {}", e.getMessage());
			throw new IllegalStateException(e);
		}
		log.debug("Successfully updated {}", userDatabase);
		return Optional.of(new FileSystemResource(database));
	}


	@Override
	public OperationResult restoreVersion(final String username, final Integer version, boolean backup) {
		log.debug("Restoring {} version {} ...", userDatabase, version);
		final Optional<UserDatabaseVersion> optionalDatabaseVersion = getVersion(version);
		if (!optionalDatabaseVersion.isPresent()) {
			log.warn("Failed to restore {} version {}: not found", userDatabase, version);
			return OperationResult.notFound();
		}
		final UserDatabaseVersion databaseVersion = optionalDatabaseVersion.get();
		if (backup) {
			try {
				backup();
			} catch (final IOException e) {
				final String message = String.format("An IOException occurred while backing up the current database: %s", e.getMessage());
				log.error(message, e);
				return OperationResult.error(message);
			}
		} else {
			log.trace("Backups are disabled for {}", userDatabase);
		}
		log.trace("Writing {} to {}", databaseVersion, userDatabase);
		try {
			Files.copy(databaseVersion.toPath(), userDatabase.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (final IOException e) {
			final String message = String.format("An IOException occurred while restoring the database to version %s: %s", version, e.getMessage());
			log.error(message, e);
			return OperationResult.error(message);
		}
		log.debug("Successfully restored {} version {}", userDatabase, version);
		return OperationResult.success();
	}

	@Override
	public OperationResult deleteVersion(final String username, final Integer version) {
		log.debug("Deleting {} version {} ...", userDatabase, version);
		final Optional<UserDatabaseVersion> optionalDatabaseVersion = getVersion(version);
		if (!optionalDatabaseVersion.isPresent()) {
			log.warn("Failed to delete {} version {}: not found", userDatabase, version);
			return OperationResult.notFound();
		}
		final UserDatabaseVersion databaseVersion = optionalDatabaseVersion.get();
		if (!databaseVersion.delete()) {
			return OperationResult.error(String.format("Could not delete %s", databaseVersion));
		}
		log.debug("Successfully deleted {}", databaseVersion);
		return OperationResult.success();
	}

	@Override
	public OperationResult deleteVersionRange(final String username, final Integer from, final Integer to) {
		log.debug("Deleting {} versions from {} to {}", userDatabase, from, to);
		final String[] databaseVersions = userDirectory.list(FilesystemDatabaseStoreConstants.FILENAME_FILTER);
		if (databaseVersions == null || databaseVersions.length == 0) {
			log.debug("{} has no backups", userDatabase);
			return OperationResult.success();
		}
		log.debug("{} has {} backups", userDatabase, databaseVersions.length);
		for (final String databaseVersion : databaseVersions) {
			final Optional<Integer> optionalVersion = FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(databaseVersion);
			if (optionalVersion.isPresent()) {
				final int v = optionalVersion.get();
				if (v >= from && v <= to) {
					deleteVersion(null, optionalVersion.get());
				}
			} else {
				log.warn("Could not extract database version from filename '{}'", databaseVersion);
			}
		}
		return OperationResult.success();
	}

	private Optional<Path> getDatabase(final boolean mustExist) {
		log.debug("Loading {} ...", userDatabase);
		final Path currentDatabase = Paths.get(userDirectory.getAbsolutePath(), FilesystemDatabaseStoreConstants.DATABASE_FILE_NAME);
		if (!Files.exists(currentDatabase)) {
			if (mustExist) {
				log.error("{} does not exist after initialization", userDatabase);
			}
			return Optional.empty();
		}
		log.debug("Successfully loaded {}", userDatabase);
		return Optional.of(currentDatabase);
	}

	private Optional<UserDatabaseVersion> getVersion(final Integer version) {
		final UserDatabaseVersion userDatabaseVersion = new UserDatabaseVersion(userDirectory, version);
		log.debug("Loading {}", userDatabaseVersion);
		final String databaseVersionPath = FilesystemDatabaseStoreStoreUtils.getDatabaseVersionFilename(version);
		final Path databaseVersion = Paths.get(userDirectory.getAbsolutePath(), databaseVersionPath);
		if (!Files.exists(databaseVersion)) {
			log.warn("{} does not exist", userDatabaseVersion);
			return Optional.empty();
		}
		log.debug("Successfully loaded {}", userDatabaseVersion);
		return Optional.of(userDatabaseVersion);
	}

	private DatabaseInformation _getDatabaseInformation(final String database) {
		log.trace("Reading database information of '{}'", database);
		final DatabaseInformation databaseInformation = new DatabaseInformation();
		final File databaseFile = new File(userDirectory, database);
		databaseInformation.setVersion(FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(database).orElse(null));
		databaseInformation.setLastModified(FilesystemDatabaseStoreStoreUtils.lastModified(databaseFile).orElse(null));
		return databaseInformation;
	}

	private void backup() throws IOException {
		log.debug("Backing up {} ...", userDatabase);
		final Optional<Path> optionalPath = getDatabase(true);
		if (optionalPath.isPresent()) {
			final Path currentDatabase = optionalPath.get();
			final int nextVersion = getNextDatabaseVersion();
			final UserDatabaseVersion userDatabaseVersion = new UserDatabaseVersion(userDirectory, nextVersion);
			log.trace("Backing up {} to {} ...", userDatabase, userDatabaseVersion);
			Files.copy(currentDatabase, userDatabaseVersion.toPath());
			log.debug("Successfully backed up {} to {}", userDatabase, userDatabaseVersion);
		} else {
			throw new IOException(String.format("Failed to back up %s: could not be found", userDatabase));
		}
	}

	private int getNextDatabaseVersion() {
		log.trace("Determining next version for {} ...", userDatabase);
		final String[] databaseVersions = userDirectory.list(FilesystemDatabaseStoreConstants.FILENAME_FILTER);
		if (databaseVersions == null || databaseVersions.length == 0) {
			log.trace("Next version for {} is 1 because there are no versions yet", userDatabase);
			return 1;
		}
		Arrays.sort(databaseVersions);
		final int lastVersion = FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(databaseVersions[databaseVersions.length - 1]).orElse(0);
		final int nextVersion = lastVersion + 1;
		log.trace("Last version of {} is {} ", userDatabase, lastVersion);
		log.trace("Next version of {} is {}", userDatabase, nextVersion);
		return nextVersion;
	}

	void purge() {
		log.debug("Purging {} ...", userDirectory);
		if (!FilesystemDatabaseStoreStoreUtils.deleteDirectory(userDirectory)) {
			log.error("Failed to purge user directory '{}'", userDirectory.getAbsolutePath());
		} else {
			log.debug("Successfully purged {}", userDirectory);
		}
	}
}
