package at.palata.keepass.webstorage.store.impl.filesystem;

import lombok.ToString;

import java.io.File;

@ToString(callSuper = true)
class UserDatabase extends File {

	UserDatabase(final UserDirectory parent, final String child) {
		super(parent, child);
	}
}
