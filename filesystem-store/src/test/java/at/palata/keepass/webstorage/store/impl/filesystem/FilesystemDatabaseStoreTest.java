package at.palata.keepass.webstorage.store.impl.filesystem;

import at.palata.keepass.webstorage.api.auth.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.Optional;
import java.util.UUID;

public class FilesystemDatabaseStoreTest {

	private static final File databaseDirectory = new File("target");

	private String username;
	private FilesystemDatabaseStore filesystemDatabaseStore;

	@Before
	public void setup() {
		final FilesystemDatabaseStoreProperties filesystemDatabaseStoreProperties = new FilesystemDatabaseStoreProperties();
		filesystemDatabaseStoreProperties.setDirectory(new PathResource(databaseDirectory.getName()));
		filesystemDatabaseStoreProperties.initialize();

		this.username = UUID.randomUUID().toString();

		final User user = new User();
		user.setUsername(this.username);
		this.filesystemDatabaseStore = new FilesystemDatabaseStore(filesystemDatabaseStoreProperties, user);
	}

	@Test
	public void test_initialized() {
		this.filesystemDatabaseStore.initialize();

		final File userDir = new File(databaseDirectory, this.username);
		Assert.assertTrue("User Directory must exist after initialization", userDir.exists());
	}

	@Test
	public void test_getDatabase() {
		this.filesystemDatabaseStore.initialize();

		final Optional<Resource> optionalDatabase = this.filesystemDatabaseStore.getDatabase(null);
		Assert.assertTrue("Initial database must exist after User Directory initialization", optionalDatabase.isPresent());
	}

	@After
	public void cleanup() {
		this.filesystemDatabaseStore.purge();
	}
}
