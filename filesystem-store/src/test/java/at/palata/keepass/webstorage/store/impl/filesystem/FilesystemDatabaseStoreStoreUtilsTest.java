package at.palata.keepass.webstorage.store.impl.filesystem;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public class FilesystemDatabaseStoreStoreUtilsTest {

	final File target = new File("target");

	private final Integer version_a = 1;
	private final String version_a_filename = "database.000000001.kdbx";

	private final Integer version_b = 65535;
	private final String version_b_filename = "database.000065535.kdbx";

	private final Integer version_c = 123456789;
	private final String version_c_filename = "database.123456789.kdbx";

	@Before
	public void setup() {
		Assert.assertTrue(target.exists());
	}

	@Test
	public void test_getDatabaseVersionFilename() {
		Assert.assertEquals("must match", version_a_filename, FilesystemDatabaseStoreStoreUtils.getDatabaseVersionFilename(version_a));
		Assert.assertEquals("must match", version_b_filename, FilesystemDatabaseStoreStoreUtils.getDatabaseVersionFilename(version_b));
		Assert.assertEquals("must match", version_c_filename, FilesystemDatabaseStoreStoreUtils.getDatabaseVersionFilename(version_c));
	}

	@Test
	public void test_getDatabaseVersion() {
		final Optional<Integer> optionalVersionA = FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(version_a_filename);
		Assert.assertTrue("Must extract version", optionalVersionA.isPresent());
		optionalVersionA.ifPresent(version -> Assert.assertEquals("Must extract version from filename", version_a, version));

		final Optional<Integer> optionalVersionB = FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(version_b_filename);
		Assert.assertTrue("Must extract version", optionalVersionB.isPresent());
		optionalVersionB.ifPresent(version -> Assert.assertEquals("Must extract version from filename", version_b, version));

		final Optional<Integer> optionalVersionC = FilesystemDatabaseStoreStoreUtils.getDatabaseVersion(version_c_filename);
		Assert.assertTrue("Must extract version", optionalVersionC.isPresent());
		optionalVersionC.ifPresent(version -> Assert.assertEquals("Must extract version from filename", version_c, version));
	}

	@Test
	public void test_lastModified() {
		final Optional<LocalDateTime> optionalLastModified = FilesystemDatabaseStoreStoreUtils.lastModified(new File("target"));
		Assert.assertTrue("Must get last modified timestamp", optionalLastModified.isPresent());
		optionalLastModified.ifPresent(lastModified -> Assert.assertTrue("valid last modified", lastModified.isBefore(LocalDateTime.now())));
		optionalLastModified.ifPresent(lastModified -> Assert.assertFalse("valid last modified", lastModified.isAfter(LocalDateTime.now())));
	}

	@Test
	public void test_deleteDirectory() {
		final File rootDir = new File(target, UUID.randomUUID().toString());
		Assert.assertTrue(rootDir.mkdirs());
		final File dir = new File(rootDir,UUID.randomUUID().toString() + File.separatorChar + UUID.randomUUID().toString());
		Assert.assertTrue(dir.mkdirs());
		FilesystemDatabaseStoreStoreUtils.deleteDirectory(rootDir);
		Assert.assertFalse(dir.exists());
		Assert.assertFalse(rootDir.exists());
	}
}
