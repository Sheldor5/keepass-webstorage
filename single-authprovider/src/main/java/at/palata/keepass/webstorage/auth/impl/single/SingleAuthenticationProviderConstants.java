package at.palata.keepass.webstorage.auth.impl.single;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;

class SingleAuthenticationProviderConstants {
	static final String PROPERTY_ENABLE_VALUE = "single";
	static final String PROPERTY_PREFIX = KeePassWebStoragePropertiesConstants.PROPERTY_PREFIX + ".auth-provider." + PROPERTY_ENABLE_VALUE;
}
