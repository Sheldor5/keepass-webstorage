package at.palata.keepass.webstorage.auth.impl.single;

import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = SingleAuthenticationProviderConstants.PROPERTY_ENABLE_VALUE)
@ConfigurationProperties(prefix = SingleAuthenticationProviderConstants.PROPERTY_PREFIX)
public class SingleAuthenticationProviderProperties {

	@NotNull
	private String username = "admin";

	@NotNull
	private String password = "admin";

	@NotNull
	private Boolean isAdmin = true;

	@NotNull
	private Boolean backupsEnabled = true;

	@NotNull
	private Boolean updatesAllowed = true;

	private int allowedAttempts = 5;

	private int lockedMinutes = 5;

}
