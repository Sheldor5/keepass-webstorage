package at.palata.keepass.webstorage.auth.impl.single;


import at.palata.keepass.webstorage.api.config.KeePassWebStoragePropertiesConstants;
import at.palata.keepass.webstorage.api.auth.AuthenticationProvider;
import at.palata.keepass.webstorage.api.auth.SecurityConstants;
import at.palata.keepass.webstorage.api.auth.User;
import at.palata.keepass.webstorage.api.store.OperationResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@ConditionalOnProperty(name = KeePassWebStoragePropertiesConstants.AUTH_PROVIDER_TYPE_PROPERTY, havingValue = SingleAuthenticationProviderConstants.PROPERTY_ENABLE_VALUE)
@EnableConfigurationProperties(SingleAuthenticationProviderProperties.class)
@Slf4j
public class SingleAuthenticationProvider extends AuthenticationProvider {

	private final SingleAuthenticationProviderProperties properties;
	private final User user;
	private final AtomicInteger wrongPasswordCount = new AtomicInteger(0);
	private LocalDateTime lockedUntil = null;

	@Autowired
	public SingleAuthenticationProvider(final SingleAuthenticationProviderProperties properties) {
		this.properties = properties;
		user = new User();
		user.setUsername(properties.getUsername());
		user.setPassword(properties.getPassword());
		user.setBackupsEnabled(properties.getBackupsEnabled());
		user.setUpdatesAllowed(properties.getUpdatesAllowed());
		user.setIsAdmin(properties.getIsAdmin());
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (isLocked()) {
			return null;
		}
		if (authentication != null) {
			final String username = authentication.getName();
			final String password = authentication.getCredentials().toString();
			if (user.getUsername().equals(username)) {
				if (user.getPassword().equals(password)) {
					resetLock();
					return new UsernamePasswordAuthenticationToken(username, password, Collections.singletonList(new SimpleGrantedAuthority(SecurityConstants.ADMIN_AUTHORITY)));
				} else {
					onWrongPassword();
				}
			}
		}
		return null;
	}

	private boolean isLocked() {
		if (lockedUntil != null) {
			if (lockedUntil.isAfter(LocalDateTime.now(ZoneId.systemDefault()))) {
				log.debug("User[{}] is still locked until {}", user.getUsername(), lockedUntil);
				return true;
			} else {
				log.info("User[{}] unlocked, was locked until {}", user.getUsername(), lockedUntil);
				resetLock();
			}
		}
		return false;
	}

	private void onWrongPassword() {
		final int attempt = wrongPasswordCount.incrementAndGet();
		log.trace("User[{}] entered a wrong password {} times", user.getUsername(), attempt);
		if (attempt >= properties.getAllowedAttempts()) {
			lockedUntil = LocalDateTime.now(ZoneId.systemDefault()).plusMinutes(properties.getLockedMinutes());
			log.info("User[{}] entered a wrong password too often and is now locked until {}", user.getUsername(), lockedUntil);
		}
	}

	private void resetLock() {
		wrongPasswordCount.set(0);
		lockedUntil = null;
	}

	@Override
	public Optional<User> getUser(final String username) {
		if (user.getUsername().equals(username)) {
			return Optional.of(user.clone());
		}
		return Optional.empty();
	}

	@Override
	public OperationResult createUser(final User user) {
		return OperationResult.notImplemented("The Simple AuthProvider does not support creating new users!");
	}

	@Override
	public OperationResult updateUser(final User newAttributes) {
		return OperationResult.notImplemented("The Simple AuthProvider does not support updating users!");
	}

	@Override
	public OperationResult changeUserPassword(String username, String oldPassword, String newPassword) {
		return OperationResult.notImplemented("The Simple AuthProvider does not support changing user passwords!");
	}
}
