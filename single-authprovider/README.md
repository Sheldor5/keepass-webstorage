# Single-AuthenticationProvider for [_KeePass WebStorage_](https://gitlab.com/Sheldor5/keepass-webstorage)

![KeePass WebStorage](../readme/home.png)

This Authentication Provider only allows a single user. 

### Configuration

In the main `application.properties` you have to configure the `type` as `single`:

```properties
keepass.webstorage.auth-provider-type=single
```

By default, this will enable the `admin` user with password `admin`.

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) **Please change both the username and password!**

The configured user will always be an administrator.

##### Change Default User

Following properties can be used to change the user:

```properties
keepass.webstorage.auth-provider.single.username=admin
keepass.webstorage.auth-provider.single.password=admin
keepass.webstorage.auth-provider.single.backups-enabled=true
keepass.webstorage.auth-provider.single.updates-allowed=true
```

##### Password Policies

You can configure the allowed attempts for wrong passwords and the lock time for the locked user (default values):

```properties
keepass.webstorage.auth-provider.single.allowed-attempts=5
keepass.webstorage.auth-provider.single.locked-minutes=5
```
